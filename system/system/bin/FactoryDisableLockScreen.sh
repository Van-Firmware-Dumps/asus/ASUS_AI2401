#!/system/bin/sh
# $1 == 1 is ON, $1 == 0 is OFF

is_factory=`getprop ro.boot.ftm`

if [ "$is_factory" == "1" ];then
	echo "Disabling lock screen"
	input keyevent 62
	sleep 2
	locksettings set-disabled true
	service call SurfaceFlinger 1035 i32 2
else
	echo "Not in factory build"
fi
