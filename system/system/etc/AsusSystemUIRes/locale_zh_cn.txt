﻿[{"wallpaper":"default_slideshow_wallpaper01","title":"金黄橘​","description":"耀眼的光芒照亮未来之路。​","author":""},
{"wallpaper":"default_slideshow_wallpaper02","title":"尊贵蓝​","description":"感受每一处细节的低调奢华美感。​","author":""},
{"wallpaper":"default_slideshow_wallpaper03","title":"简朴绿","description":"简约纯净的绿意质感生活。","author":""},
{"wallpaper":"default_slideshow_wallpaper04","title":"活跃灰​","description":"为生活增添创意乐趣。","author":""},
{"wallpaper":"default_slideshow_wallpaper05","title":"浅褐沙","description":"品味生活中的美好细节。","author":""},
{"wallpaper":"default_slideshow_wallpaper06","title":"神秘紫​","description":"花瓣线条敲荡出迷蒙的情境。","author":""},
{"wallpaper":"default_slideshow_wallpaper07","title":"彩虹环","description":"缤纷色彩交织融合成美丽的梦想。","author":""},
{"wallpaper":"default_slideshow_wallpaper08","title":"鲜洋红","description":"注入热情活力，重新展现自我。","author":""}]
