#!/system/bin/sh

echo "PCBID TEST: +++"

rm /data/data/pcbid_status_str_tmp

PROP_STAGE=$(getprop ro.boot.id.stage)
PROP_PROJECT=$(getprop ro.boot.id.prj)
PROP_CAM=$(getprop ro.boot.id.cam)

STAGE=
PROJECT=

if [ $PROP_STAGE -lt 4 ]; then #HW Stage < ER2
	case $PROP_PROJECT in
		"0" )
			PROJECT='AI2401_Miami_Pro'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
		"1" )
			PROJECT='AI2401_Miami_Ultimate'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
		"2" )
			PROJECT='AI2401_Havana'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
		*)
			PROJECT='UNKNOW('$PROP_PROJECT')'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
	esac
else
    case $PROP_PROJECT in
		"0" )
		    if [ "$PROP_CAM" -eq "0" ]; then
				PROJECT='AI2401_Miami_Entry'				
		    else
		        PROJECT='AI2401_Miami_Standard'
		    fi
		    echo "PCBID TEST: PROJECT="$PROJECT
			;;
		"1" )
			PROJECT='AI2401_Miami_Pro'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
		"2" )
			PROJECT='AI2401_Havana'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
		*)
			PROJECT='UNKNOW('$PROP_PROJECT')'
			echo "PCBID TEST: PROJECT="$PROJECT
			;;
	esac
fi

if [ "$PROP_PROJECT" -eq "2" ]; then #Havana
	case $PROP_STAGE in
    	"2" )
			STAGE='SR'
			echo "PCBID TEST: STAGE="$STAGE
			;;
	    "3" )
			STAGE='SR2'
			echo "PCBID TEST: STAGE="$STAGE
			;;
	    "5" )
			STAGE='ER'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		*)
			STAGE='UNKNOW('$PROP_STAGE')'
			echo "PCBID TEST: STAGE="$STAGE
			;;
	esac
else #Miami
	case $PROP_STAGE in
		"0" )
			STAGE='EVB'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		"1" )
			STAGE='SR1'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		"2" )
			STAGE='SR2'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		"3" )
			STAGE='ER1'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		"4" )
			STAGE='ER2'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		"5" )
			STAGE='PR'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		"6" )
			STAGE='MP'
			echo "PCBID TEST: STAGE="$STAGE
			;;
		*)
			STAGE='UNKNOW('$PROP_STAGE')'
			echo "PCBID TEST: STAGE="$STAGE
			;;
	esac
fi

echo $PROJECT"_"$STAGE > /data/data/pcbid_status_str_tmp
chmod 00777 /data/data/pcbid_status_str_tmp

echo "PCBID TEST: ---"
