wifi_mac=`sed -n '1 p' /vendor/factory/wlan_mac.bin`
wifi_mac=${wifi_mac//Intf0MacAddress=/ }
setprop ro.vendor.wifimac $wifi_mac
wifi_mac=`sed -n '2 p' /vendor/factory/wlan_mac.bin`
wifi_mac=${wifi_mac//Intf1MacAddress=/ }
setprop ro.vendor.wifimac_2 $wifi_mac

setprop vendor.wifi.version.driver WLAN.HMT.2.0-01921-QCAHMTSWPL_V1.0_V2.0_SILICONZ-1.57040.5

# BT
setprop vendor.bt.version.driver BTFW.HAMILTON.2.0.1-00278-PATCHZ-1
setprop ro.vendor.btmac `btnvtool -x 2>&1`
